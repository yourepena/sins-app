import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { SideBarService } from '../services/side-bar.service';



@Component({
  selector: 'app-header-page',
  templateUrl: './header-page.component.html',
  styleUrls: ['./header-page.component.css']
})
export class HeaderPageComponent implements OnInit {

  constructor(location: Location, router: Router, sideBarService: SideBarService) {
    router.events.subscribe((val) => {
      sideBarService.blockHeader(location.path());
    });
  }

  ngOnInit() {
  }

}
