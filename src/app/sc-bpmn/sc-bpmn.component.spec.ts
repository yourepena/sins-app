import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScBpmnComponent } from './sc-bpmn.component';

describe('ScBpmnComponent', () => {
  let component: ScBpmnComponent;
  let fixture: ComponentFixture<ScBpmnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScBpmnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScBpmnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
