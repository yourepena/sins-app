import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FornecimentoDadosComponent } from './fornecimento-dados.component';

describe('FornecimentoDadosComponent', () => {
  let component: FornecimentoDadosComponent;
  let fixture: ComponentFixture<FornecimentoDadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FornecimentoDadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FornecimentoDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
