import { Component, OnInit, Output, EventEmitter, ViewChild, ContentChild, Input } from '@angular/core';



import { DatepickerOptions } from 'ng2-datepicker';
import * as ptLocale from 'date-fns/locale/pt';
import { NgForm, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-fornecimento-dados',
  templateUrl: './fornecimento-dados.component.html',
  styleUrls: ['./fornecimento-dados.component.css']
})
export class FornecimentoDadosComponent implements OnInit {

  constructor() { }

  tituloText: string;

  dateText: string;

  @Input()
  f: FormGroup;

  options: DatepickerOptions = {
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'D/MM/YYYY',
    barTitleFormat: 'MMMM YYYY',
    dayNamesFormat: 'dd',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    locale: ptLocale,
    //minDate: new Date(Date.now()), // Minimal selectable date
    //maxDate: new Date(Date.now()),  // Maximal selectable date
    barTitleIfEmpty: 'Selecione uma data',
    placeholder: 'Selecione uma data', // HTML input placeholder attribute (default: '')
    addClass: 'ss-form-control', // Optional, value to pass on to [ngClass] on the input field
    addStyle: {}, // Optional, value to pass to [ngStyle] on the input field
    fieldId: 'my-date-picker', // ID to assign to the input field. Defaults to datepicker-<counter>
    useEmptyBarTitle: false, // Defaults to true. If set to false then barTitleIfEmpty will be disregarded and a date will always be shown 
  };


 
  ngOnInit(): void {
    this.f = new FormGroup({
      'titulo': new FormControl(this.tituloText, [
        Validators.required 
      ]),
      'date': new FormControl(this.dateText, [
        Validators.required 
      ])
    });
  }



}
