import { Component, OnInit, ContentChild, AfterContentInit, ViewChild, Input } from '@angular/core';
import { TabComponent } from '../tabs/tab/tab.component';
import { TabPanelComponent } from '../tabs/tab-panel/tab-panel.component';
import { SideBarService } from '../services/side-bar.service';

import { ScModalServiceService } from '../services/sc-modal-service.service'
import { Project } from '../model/project';
import { ProjectService } from '../services/project.service';

import { ActivatedRoute } from '@angular/router';
import { Task } from '../model/task';
import { FornecimentoDadosComponent } from './fornecimento-dados/fornecimento-dados.component';


@Component({
  selector: 'app-execute-action',
  templateUrl: './execute-action.component.html',
  styleUrls: ['./execute-action.component.css']
})
export class ExecuteActionComponent implements OnInit, AfterContentInit {

  abaSelecionada: string = "aba1";

  @Input()
  project: Project;

  @Input()
  task: Task;

  @ViewChild(TabPanelComponent) tabPanel: TabPanelComponent;

  @ViewChild(FornecimentoDadosComponent) fornecimentoDados: FornecimentoDadosComponent;


  constructor(public route: ActivatedRoute, public sideBarService: SideBarService, public projectService: ProjectService,
    private modalService: ScModalServiceService) { }

  getProject(): void {
    const id = +this.route.snapshot.paramMap.get('projectid');
    this.projectService.getProjectbyId(id).subscribe(project => this.project = project);
  }

  getTask(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.projectService.getTaskbyId(id).subscribe(task => this.task = task);
  }
  log(val) { console.log(val); }


  ngOnInit() {
    this.getProject();
    this.getTask();
    this.sideBarService.removeAllSideBars();
  }

  ngAfterContentInit() { };

  public mudarAba(evento: string): void {
    console.log(this.fornecimentoDados);
    this.abaSelecionada = evento;
  }

  public irParaAba(tab: TabComponent) {
    this.tabPanel.selectTab(tab);
  }

  openModal(id: string) {
    this.task.status = 1;
    this.projectService.updateTask(this.task).subscribe(() => console.log("Atualizou a tarefa"));
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }


}
