import { Component, OnInit, ViewContainerRef, Inject, NgModule, ViewChild } from '@angular/core';
import { InjectService } from 'src/app/services/inject.service';

@Component({
  selector: 'app-investimentos',
  templateUrl: './investimentos.component.html',
  styleUrls: ['./investimentos.component.css']
})
export class InvestimentosComponent implements OnInit {


  theCheckbox = false;

  service: InjectService;

  @ViewChild('dynamic', { 
    read: ViewContainerRef 
  }) viewContainerRef: ViewContainerRef

  constructor(@Inject(InjectService) service) {
    this.service = service
  }

  ngOnInit() {
  }

  addInvestimento(): void {
    this.service.setRootViewContainerRef(this.viewContainerRef)
    this.service.addDynamicComponent()
  }

}
