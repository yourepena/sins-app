import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecuteActionComponent } from './execute-action.component';

describe('ExecuteActionComponent', () => {
  let component: ExecuteActionComponent;
  let fixture: ComponentFixture<ExecuteActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecuteActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecuteActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
