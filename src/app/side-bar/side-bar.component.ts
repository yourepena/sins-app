import { AfterViewInit, Component, OnInit, Input } from '@angular/core';

import { SideBarService } from '../services/side-bar.service';
import { Router } from '@angular/router';
import { Project } from '../model/project'

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent  {

  @Input() project: Project;

  constructor(private router: Router, public sideBarService: SideBarService) { }

/*   gotToDetail() {
    var element = (<HTMLInputElement>document.getElementById("projectidhidden"));
    this.router.navigate(['/detail/' + element.value]);
    this.sideBarService.hiddeSideBar();
  }
 */
  ngOnInit() {

  }



}
