import { Component, ViewChild, ComponentFactoryResolver, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { SideBarDirective } from './side-bar.directive';
import { SideBarComponent } from './side-bar/side-bar.component'
import { ProjectService } from './services/project.service';


import { Project } from './model/project'
import { InjectService } from './services/inject.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css',
   '../../node_modules/bpmn-js/dist/assets/bpmn-font/css/bpmn.css',
   '../../node_modules/bpmn-js/dist/assets/diagram-js.css'
]
})
export class AppComponent implements OnInit {
  title = 'sins';

  @ViewChild(SideBarDirective) sideBarDirective: SideBarDirective;

 /*  constructor(@Inject(ViewContainerRef) viewContainerRef, @Inject(InjectService) service, private componentFactoryResolver: ComponentFactoryResolver, public projectService: ProjectService) {
    service.setRootViewContainerRef(viewContainerRef)
    service.addDynamicComponent()
  } */

  constructor(private componentFactoryResolver: ComponentFactoryResolver, public projectService: ProjectService) {
  }

  loadcomponent(project: Project) {
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(SideBarComponent);

    let viewContainerRef = this.sideBarDirective.viewContainerRef;
    viewContainerRef.clear();

    let componentRef = viewContainerRef.createComponent(componentFactory);

    (<SideBarComponent>componentRef.instance).project = project;
  }

  ngOnInit() {
    // this.loadcomponent();
  }


}
