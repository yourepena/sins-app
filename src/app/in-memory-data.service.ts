import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Project } from './model/project';
import { Task } from './model/task';
import { User } from './model/user';
import { Role } from './model/role';


export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const projects = [
      { id: 1, program_name: 'CooperJovem', name: 'Mr. Nice', progress: 10, owner: 1, status: "Atrasado", dataIn: "01/12/2018", dataOut: "31/12/2018", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." },
      { id: 2, program_name: 'CooperJovem', name: 'Narco', progress: 0, owner: 1, status: "Em dia", dataIn: "01/12/2018", dataOut: "31/12/2018", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." },
      { id: 3, program_name: 'CooperJovem', name: 'Bombasto', progress: 80, owner: 1, status: "Em dia", dataIn: "01/12/2018", dataOut: "31/12/2018", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." },
      { id: 4, program_name: 'CooperJovem', name: 'Celeritas', progress: 20, owner: 1, status: "Em dia", dataIn: "01/12/2018", dataOut: "31/12/2018", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."	},
      { id: 5, program_name: 'CooperJovem', name: 'Magneta', progress: 50, owner: 1, status: "Em dia", dataIn: "01/12/2018", dataOut: "31/12/2018", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."	}
    ];

    const tasks = [
      {
        id: 1,
        taskName: "Tarefa 1",
        taskDescription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        taskDate: "01/12/2018",
        taskUser: {
          id: 1,
          iduser: 1,
          roleName: "Responsável pela ação"
        },
        project: 1,
        status: 2
      },{
        id: 2,
        taskName: "Tarefa 2",
        taskDescription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        taskDate: "01/12/2018",
        taskUser: {
          id: 1,
          iduser: 2,
          roleName: "Responsável pela ação"
        },
        project: 1,
        status: 2
      },{
        id: 3,
        taskName: "Tarefa 3",
        taskDescription: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        taskDate: "01/12/2018",
        taskUser: {
          id: 1,
          iduser: 3,
          roleName: "Responsável pela ação"
        },
        project: 1,
        status: 2
      }
    ];

    const users = [
      {
        id: 1,
        userName: "Rodrigo UX",
        email: "rodrigo.bezerra@fornecedores.sicoob.com.br",
        cpf: "000.000.000-00",
        passwordEncryted: "OSDsjdfsdoihfio386237y",
        status: "Ativo",
        post: "UX Designer"
      },{
        id: 2,
        userName: "João Master",
        email: "joao.marangon@fornecedores.sicoob.com.br",
        cpf: "000.000.000-00",
        passwordEncryted: "OSDsjdfsdoihfio386237y",
        status: "Ativo",
        post: "Scrum Master"
      },{
        id: 3,
        userName: "Youre Dev",
        email: "youre.fernandez@fornecedores.sicoob.com.br",
        cpf: "000.000.000-00",
        passwordEncryted: "OSDsjdfsdoihfio386237y",
        status: "Ativo",
        post: "Dev"
      }
    ];

    return {projects, tasks, users};
  }

  // Overrides the genId method to ensure that a project always has an id.
  // If the projects array is empty,
  // the method below returns the initial number (11).
  // if the projects array is not empty, the method below returns the highest
  // project id + 1.
  genId<T extends Project | Task | User>(myTable: T[]): number {
  return myTable.length > 0 ? Math.max(...myTable.map(t => t.id)) + 1 : 11;
}
}
