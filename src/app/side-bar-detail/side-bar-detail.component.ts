import { Component, OnInit } from '@angular/core';
import { SideBarService } from '../services/side-bar.service';


@Component({
  selector: 'app-side-bar-detail',
  templateUrl: './side-bar-detail.component.html',
  styleUrls: ['./side-bar-detail.component.css']
})
export class SideBarDetailComponent implements OnInit {

    constructor(public sideBarService: SideBarService) { }

  ngOnInit() {
  }

}
