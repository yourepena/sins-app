import {  AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-menu-horizontal',
  templateUrl: './menu-horizontal.component.html',
  styleUrls: ['./menu-horizontal.component.css']
})
export class MenuHorizontalComponent implements AfterViewInit {

  @ViewChild("navbar", {read: ElementRef}) navbar: ElementRef;


  constructor() { }



  ngOnInit() {
  }

  ngAfterViewInit(): void {
      // console.log(this.navbar.nativeElement.textContent);
  }

}
