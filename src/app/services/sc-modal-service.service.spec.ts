import { TestBed } from '@angular/core/testing';

import { ScModalServiceService } from './sc-modal-service.service';

describe('ScModalServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScModalServiceService = TestBed.get(ScModalServiceService);
    expect(service).toBeTruthy();
  });
});
