import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SideBarService {

  sidebarEvent(id: number): void {

  /*   if (id !== undefined) {
      var element2 = (<HTMLInputElement>document.getElementById("projectidhidden"));
      if (element2 !== null) {
        element2.value = ''+id;
      }
    }

    var node = document.createElement("app-side-bar");
    document.getElementById("sideBarResume").appendChild(node); */

    var element = document.getElementById("app");
    element.classList.remove("app-sidebar-detail-event");
    element.classList.toggle("app-sidebar-event");

  }

  removeAllSideBars(): void {
    var element = document.getElementById("app");
    element.classList.remove("app-sidebar-event");
    element.classList.remove("app-sidebar-detail-event");
  }

  sidebarEventDetail(): void {
    var element = document.getElementById("app");
    element.classList.remove("app-sidebar-event");
    element.classList.toggle("app-sidebar-detail-event");
    var element1 = document.getElementById("projectDetail");
    element1.classList.remove("hidde");
  }

  hiddeSideBar(): void {
    var element = document.getElementById("app");
    element.classList.remove("app-sidebar-event");
  }

  blockHeader(route: string): void {
    if (route.includes('detail') && !route.includes('action') ) {
      var element = document.getElementById("fluxograma");
      element.classList.remove("hidde");
    }
  }


}
