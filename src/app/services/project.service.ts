import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Project } from '../model/project';
import { Task } from '../model/task';
import { User } from '../model/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class ProjectService {

  private projectUrl = 'api/projects';  // URL to web api
  private taskUrl = 'api/tasks';  // URL to web api
  private userUrl = 'api/users';  // URL to web api

  constructor(
    private http: HttpClient) { }

  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.projectUrl)
      .pipe(catchError(this.handleError('getProjects', [])));
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl)
      .pipe(catchError(this.handleError('getUsers', [])));
  }

  getUserbyId(id: number): Observable<User> {
    const url = `${this.userUrl}/${id}`;
    return this.http.get<User>(url).pipe(
      catchError(this.handleError<User>(`getUserbyId id=${id}`))
    );
  }

  getProjectbyId(id: number): Observable<Project> {
    const url = `${this.projectUrl}/${id}`;
    return this.http.get<Project>(url).pipe(
      catchError(this.handleError<Project>(`getProjectbyId id=${id}`))
    );
  }


  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.taskUrl)
      .pipe(catchError(this.handleError('getTasks', [])));
  }

  getTaskbyId(id: number): Observable<Task> {
    const url = `${this.taskUrl}/${id}`;
    return this.http.get<Task>(url).pipe(
      catchError(this.handleError<Task>(`getTaskById id=${id}`))
    );
  }

  getTasksByStatus(status: number): Observable<Task[]> {
    const url = `${this.taskUrl}/?status=${status}`;
    return this.http.get<Task[]>(url).pipe(catchError(this.handleError('getTasks', [])));
  }

  updateTask(task: Task): Observable<any> {
    return this.http.put(this.taskUrl, task, httpOptions).pipe(
      catchError(this.handleError<any>('updateHero'))
    );
  }



  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


}
