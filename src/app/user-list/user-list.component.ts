import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '../services/users-api.service';
import { User } from '../model/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  public usuarios: User[]

  constructor(private usersApi: UsersApiService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.usersApi.getUsers()
    .subscribe(usuarios => this.usuarios = usuarios);
  }

}
