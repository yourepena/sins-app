import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { NgDatepickerModule } from 'ng2-datepicker';

import {NgxMaskModule} from 'ngx-mask'
import { NgxCurrencyModule } from "ngx-currency";


import { AppComponent } from './app.component';
import { MenuHorizontalComponent } from './menu-horizontal/menu-horizontal.component';
import { MenuVerticalComponent } from './menu-vertical/menu-vertical.component';
import { HeaderPageComponent } from './header-page/header-page.component';
import { AppContentComponent } from './app-content/app-content.component';
import { ProjectsResumeComponent } from './app-content/projects-resume/projects-resume.component';
import { DatatableProjectsResumeComponent } from './app-content/datatable-projects-resume/datatable-projects-resume.component';
import { DatatableProjectsResumeHeaderComponent } from './app-content/datatable-projects-resume-header/datatable-projects-resume-header.component';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { HttpClientModule,  HttpClient, HttpHandler }    from '@angular/common/http';
import { SideBarComponent } from './side-bar/side-bar.component';
// import { EasyPieChartModule } from 'ng2modules-easypiechart';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { AppRoutingModule } from './app-routing.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { PerfilImgComponent } from './perfil-img/perfil-img.component';
import { SideBarDetailComponent } from './side-bar-detail/side-bar-detail.component';
import {BreadcrumbsModule} from "ng6-breadcrumbs";
import { CardTaskComponent } from './project-detail/card-task/card-task.component';
import { ProjectDetailHeaderComponent } from './project-detail/project-detail-header/project-detail-header.component';
import { SideBarDirective } from './side-bar.directive';
import { ExecuteActionComponent } from './execute-action/execute-action.component';
import { TabPanelComponent } from './tabs/tab-panel/tab-panel.component';
import { TabComponent } from './tabs/tab/tab.component';
import { FornecimentoDadosComponent } from './execute-action/fornecimento-dados/fornecimento-dados.component';
import { InvestimentosComponent } from './execute-action/investimentos/investimentos.component';
import { ObservacoesComponent } from './execute-action/observacoes/observacoes.component';
import { ScModalComponent } from './sc-modal/sc-modal.component';
import { InvestimentoComponent } from './execute-action/investimentos/investimento/investimento.component';
import { ScBpmnComponent } from './sc-bpmn/sc-bpmn.component';
import { FormioModule } from 'angular-formio';
import { UserListComponent } from './user-list/user-list.component';



@NgModule({
  declarations: [
    AppComponent,
    MenuHorizontalComponent,
    MenuVerticalComponent,
    HeaderPageComponent,
    AppContentComponent,
    ProjectsResumeComponent,
    DatatableProjectsResumeComponent,
    DatatableProjectsResumeHeaderComponent,
    SideBarComponent,
    ProjectDetailComponent,
    PerfilImgComponent,
    SideBarDetailComponent,
    CardTaskComponent,
    ProjectDetailHeaderComponent,
    SideBarDirective,
    ExecuteActionComponent,
    TabPanelComponent,
    TabComponent,
    FornecimentoDadosComponent,
    InvestimentosComponent,
    ObservacoesComponent,
    ScModalComponent,
    InvestimentoComponent,
    ScBpmnComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    NgDatepickerModule,
    FormsModule,
    HttpClientModule,
    BreadcrumbsModule,
    NgxMaskModule.forRoot(

    ),
    NgxCurrencyModule,
   /*  HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ), */
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    AppRoutingModule
  ],
  providers: [],
  entryComponents: [ SideBarComponent, InvestimentoComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
