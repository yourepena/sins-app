export class Project {
  id: number;
  program_name: string;
  name: string;
  progress: number;
  owner: number;
  status: string;
  dataIn: string;
  dataOut: string;
  description: string;
}
