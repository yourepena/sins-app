import {Role} from "./role"

export class Task {
  id: number;
  taskName: string;
  taskDescription: string;
  taskDate: string;
  taskUser: Role;
  project: number;
  status: number;
}
