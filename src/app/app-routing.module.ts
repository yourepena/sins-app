import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppContentComponent }   from './app-content/app-content.component';
import { ProjectDetailComponent }      from './project-detail/project-detail.component';
import { ExecuteActionComponent }      from './execute-action/execute-action.component';
import { ScBpmnComponent } from './sc-bpmn/sc-bpmn.component';

// const routes: Routes = [
//   { path: '', redirectTo: '/project', pathMatch: 'full' },
//   { path: 'project', component: AppContentComponent },
//   { path: 'detail/:id', component: ProjectDetailComponent }
// ];

export const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {
        path: 'home',
        component: AppContentComponent,
        data: {
            breadcrumb: 'Inicio > Dashboard'
        }
    },
    {
        path: 'detail/:id',
        component: ProjectDetailComponent,
        data: {
            breadcrumb: 'Inicio > Dashboard > Projeto1 > Ações do Projeto'
        }
    },
    {
        path: 'detail/:projectid/action/:id',
        component: ExecuteActionComponent,
        data: {
            breadcrumb: 'Inicio > Dashboard > Projeto1 > Ações do Projeto > Informações'
        }
    },
    {
        path: 'bpmn/:id',
        component: ScBpmnComponent,
        data: {
            breadcrumb: 'Inicio > Dashboard > Projeto1 > Ações do Projeto'
        }
    }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
