import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appSideBar]'
})
export class SideBarDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }


}
