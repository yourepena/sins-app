import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatatableProjectsResumeComponent } from './datatable-projects-resume.component';

describe('DatatableProjectsResumeComponent', () => {
  let component: DatatableProjectsResumeComponent;
  let fixture: ComponentFixture<DatatableProjectsResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatatableProjectsResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatableProjectsResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
