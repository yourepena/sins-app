import { Component, OnInit } from '@angular/core';
import { Project } from '../../model/project';
import { ProjectService } from '../../services/project.service';
import { SideBarService } from '../../services/side-bar.service';
import {AppComponent} from '../../app.component'

@Component({
  selector: 'app-datatable-projects-resume',
  templateUrl: './datatable-projects-resume.component.html',
  styleUrls: ['./datatable-projects-resume.component.css']
})
export class DatatableProjectsResumeComponent implements OnInit {

  projects: Project[];
  project: Project;
  constructor(private projectService: ProjectService, private sideBarService: SideBarService, private appComponent: AppComponent) { }


  sidebarEvent(id: number): void {
    this.getProjectbyId(id);
  }

  getProjects(): void {
    this.projectService.getProjects()
    .subscribe(projects => this.projects = projects);
  }


  getProjectbyId(id: number): void {
    this.projectService.getProjectbyId(id).subscribe(project => this.appComponent.loadcomponent(project) );
    this.sideBarService.sidebarEvent(id);
  }

  ngOnInit() {
    this.getProjects();
  }


}
