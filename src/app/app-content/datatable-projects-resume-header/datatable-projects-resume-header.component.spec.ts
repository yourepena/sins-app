import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatatableProjectsResumeHeaderComponent } from './datatable-projects-resume-header.component';

describe('DatatableProjectsResumeHeaderComponent', () => {
  let component: DatatableProjectsResumeHeaderComponent;
  let fixture: ComponentFixture<DatatableProjectsResumeHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatatableProjectsResumeHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatableProjectsResumeHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
