import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsResumeComponent } from './projects-resume.component';

describe('ProjectsResumeComponent', () => {
  let component: ProjectsResumeComponent;
  let fixture: ComponentFixture<ProjectsResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
