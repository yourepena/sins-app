import { Component, OnInit, Input } from '@angular/core';

import { ProjectService } from '../../services/project.service';
import { Task } from '../../model/task';
import { User } from '../../model/user';
import { SideBarService } from '../../services/side-bar.service';
import { Project } from 'src/app/model/project';

@Component({
  selector: 'app-card-task',
  templateUrl: './card-task.component.html',
  styleUrls: ['./card-task.component.css']
})
export class CardTaskComponent implements OnInit {

  @Input()
  task: Task;

  @Input()
  project: Project;

  public user: User;

  constructor(private projectService: ProjectService, public sideBarService: SideBarService) {}

  getUserbyId(): void {
    this.projectService.getUserbyId(this.task.taskUser.iduser)
    .subscribe(user => this.user = user);
  }

  ngOnInit() {
    this.getUserbyId();
  }



}
