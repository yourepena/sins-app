import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProjectService }  from '../../services/project.service';

import { Project } from '../../model/project';

@Component({
  selector: 'app-project-detail-header',
  templateUrl: './project-detail-header.component.html',
  styleUrls: ['./project-detail-header.component.css']
})
export class ProjectDetailHeaderComponent implements OnInit {

  @Input() project: Project;

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getProjectbyId();
  }

  getProjectbyId(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.projectService.getProjectbyId(id)
    .subscribe(project => this.project = project);
  }

}
