import { Component, OnInit, Input } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Task } from '../model/task';
import { ProjectService } from '../services/project.service';

import { SideBarService } from '../services/side-bar.service';
import { Project } from '../model/project';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  @Input() project: Project;
  tasks: Task[];
  tasksConcluidas: Task[];
  tasksAtrasadas: Task[];
  tasksRevisarDados: Task[];

  totalRevisarDados: number;
  totalAtrasadas: number;
  totalConcluida: number;
  total: number;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public projectService: ProjectService,
    public sideBarService: SideBarService
  ) { }

  getTasks(): void {
    this.projectService.getTasks()
      .subscribe(tasks => this.countTotal(tasks) );
  }

  getTasksConcluidas() {
    this.projectService.getTasksByStatus(1).subscribe(tasksConcluidas =>  this.countConcluidas(tasksConcluidas) );
  }
  getTasksAtrasadas() {
    this.projectService.getTasksByStatus(2).subscribe(tasksAtrasadas =>  this.countAtrasadas(tasksAtrasadas));
  }
  getTasksRevisarDados() {
    this.projectService.getTasksByStatus(3).subscribe(tasksRevisarDados =>  this.countRevisarDados(tasksRevisarDados) );
  }

  getProject(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.projectService.getProjectbyId(id).subscribe(project => this.project = project);
  }

  countTotal( tasks: Task[]) {
    this.tasks = tasks;
    this.total = tasks.length;
  }

  countAtrasadas( tasks: Task[]) {
    this.tasksAtrasadas = tasks;
    this.totalAtrasadas = tasks.length;
  }

  countConcluidas( tasks: Task[]) {
    this.tasksConcluidas = tasks;
    this.totalConcluida = tasks.length;
  }

  countRevisarDados( tasks: Task[]) {
    this.tasksRevisarDados = tasks;
    this.totalRevisarDados = tasks.length;
  }

  ngOnInit() {
    this.sideBarService.removeAllSideBars();
    this.getProject();
    this.getTasks();
    this.getTasksAtrasadas();
    this.getTasksConcluidas();
    this.getTasksRevisarDados();
  }

}
