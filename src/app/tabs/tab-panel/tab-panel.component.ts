import { 
    Component,
    ContentChildren, 
    QueryList, 
    Input, 
    TemplateRef, 
    AfterContentInit, 
    ContentChild
} from '@angular/core';


import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'sc-tab-panel',
  templateUrl: './tab-panel.component.html',
  styleUrls: ['./tab-panel.component.css']
})
export class TabPanelComponent implements AfterContentInit {

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  @Input()
  headerTemplate: TemplateRef<any>;

  @ContentChild(TabComponent) tab: TabComponent;

  ngAfterContentInit() {
      const selectedTab = this.tabs.find(tab => tab.selected);
      if (!selectedTab && this.tabs.first) {
          this.tabs.first.selected = true;
      }
  }

  public selectTab(tab: TabComponent) {
      this.tabs.forEach(tab => tab.selected = false);
      tab.selected = true;
      this.tab.selecionou(tab.id);
  }

  get tabsContext() {
      return {
          tabs: this.tabs
      }
  }

}
