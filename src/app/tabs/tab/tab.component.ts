import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'sc-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  status: number;

  @Input()
  total: number;

  @Output()
  aoSelecionar = new EventEmitter();

  @Input()
  selected = false;

  @Input()
  public id: string = undefined;

  constructor() {}

  ngOnInit() {}

  public selecionou(id:string) {
    this.aoSelecionar.emit(id);
  }

}
